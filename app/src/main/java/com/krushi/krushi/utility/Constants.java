package com.krushi.krushi.utility;

public class Constants {

    public static final String BEARER = "bearer";
    public static final int PAGE_SIZE1 = 12;
    public static final int PAGE_SIZE = 10;
    public static final int ADD_CART = 1;
    public static final int INCREASE_CART = 4;
    public static final int DECREASE_CART = 5;
    public static final int ADD_TO_SAVE_FOR_LATTER = 1;


    public interface BundleKey {

        String PASS_MULTIPROFILE_OBJECT = "pass_multiprofile_object";
        String RAZORPAY_ORDER_ID = "razorpay_order_id";
        String SUBSCRIPTION_AMOUNT = "subscription_amount";
        String USER_VIEW_PROFILE_ID = "go_to_profile";
        String SUB_PROFILE_ROLE = "sub_profile_role";
        String FOLLOWING_COUNT = "following_count";
        String FOLLOWERS_COUNT = "followers_count";
        String SUBSCRIBE_FLAG = "subscribe_flag";
        String RECOMMANDED_FLAG = "recommanded_flag";
        String NEW_ON_ENTIE = "new_on_entie";
        String TAB_NUMBER = "tab_no";
        String NEWONENTIE = "newonentie";
        String RECOMMENDED = "recommended";
        String EDIT_POST = "edit_post";
        String SUBSCRIPTION_TYPE_ID = "subscription_type_id";
        String PROFILE_VERIFIED = "profile_verified";
        String PROFILE_PREMIUM = "profile_premium";
        String PROFILE_RATINGS = "profile_ratings";
        String PROFILE_SUBSCRIPTION = "profile_subscription";
        String CATEGORY_PRODUCT_ID = "category_product_id";
        String CATEGORY_PRODUCT_NAME = "category_product_name";
        String PRODUCT = "product";
        String CLICKED_POSITION = "clicked_position";
        String LIST_CLICKED_POSITION = "list_clicked_position";
        String EDIT_ADDRESS_DETAILS = "edit_address_details";
        String EDIT_ADDRESS_FLAG = "edit_address_flag";
        String AREA_NOT_FOUND_FLAG = "area_not_found_flag";
        String DELIVERY_CHARGES = "delivery_charges";
        String DELIVERY_DATE = "delivery_date";
        String BAG_TOTAL_AMOUNT = "bag_total_amount";
        String BAG_TOTAL = "bag_total";
        String CONVINENCES_CHARGES = "convinences_charges";
        String BAG_TOTAL_SIZE = "bag_total_size";
        String AREA_ADDRESS_FLAG = "area_address_flag";
        String GRAIN_TYPE_FLAG = "grain_type_flag";
        String PRODUCT_ITEM_LIST = "product_item_list";
        String ORDER_ID = "order_id";
        String SKIP_AREA = "skip_area";
        String TOTAL_AMOUNT = "total_amount";
        String ADD_TO_CART_FLAG = "add_to_cart_flag";
        String LONGITUDE = "longitude";
        String LATITUDE = "latitude";
        String ADDRESS_BTN_CLICK_LOCATION = "address_button_click_location";
        String ORDER_STATUS = "order_status";
        String GRAIN_ID_LIST = "grain_id_list";
        String WALLET_BALANCE = "wallet_balance";
        String PRODUCT_ID = "product_id";
        String QUANTITY = "quantity";
        String PROFILE_PATH = "profile_path";
        String USER_DETAILS = "user_details";
        String BANNER_FLAG = "banner_flag";
        String USER_ID = "user_id";
        String USER_NAME = "user_name";
        String SIZE = "size";
        String GRINDING_TYPE = "grinding_type";
        String ORDER_NUNMBER = "order_number";
        String TIME_SLOT_ID = "time_slot_id";
        String IS_WALLET = "is_wallet";
        String DELIVERY_NOTE = "delivery_note";
        String UPDATE_WITHOUT_LOGIN_CART = "update_without_login_cart";
        String DELIVERY_TIME = "delivery_time";
        String WISHLIST_FLAG = "wishlist_flag";
        String CATEGORY_LIST = "category_list";
        String ADDRESS_LIST = "address_list";
        String POSITION = "position";
        String CATEGORY_PRODUCT_TYPE = "category_product_type";
        String REDIRECTION_ID = "redirection_id";
        String ADDRESS_ID = "address_id";
        String ADDRESS = "address";
        String SUBSCRIBTION_LIST = "SUBSCRIBTION_LIST";
        String SUBSCRIPTION_DATA = "subcription_data";
        String EDIT_ADD_BAG = "edit_add_bag";
        String IS_SUB_USE = "is_sub_use";
    }

    public interface Action {

        Integer FORGETPASSWORD = 1;
        int ADDRESS_HOME = 2;
        int ADDRESS_OFFICE = 3;
        int ADDRESS_OTHER = 4;
        int ADD_ADDRESS = 5;
        int EDIT_ADDRESS = 6;
        int ADDRESS_LIST = 7;
        int BAG_SUMMERY_PAGE = 8;
        int DELIVERY_PAGE = 9;
        int CHECKOUT_PAGE = 10;
        int RAZORPAY_ACTION = 11;
        int CHANGE_LOCATION_ACTION = 12;
        int GO_TO_CART_ACTION = 13;
        int WHOLE_GRAIN_ACTION = 14;
        int MULTI_GRAIN_ACTION = 15;
        int RECIPE_ACTION = 16;
        int GO_TO_LOGIN_PAGE = 17;
        int SAVE_AREA_ACTION = 18;
        int ADD_RECIPE = 18;
        int CALCULATE = 19;
        int RESET_RECIPE = 20;
        int CANCEL_ORDER = 21;
        int TRACK_ORDER = 22;
        int CALL = 23;
        int SET_LOCATION_LAT_LONG = 24;
        int CLEAR_FILTER = 25;
        int APPLY = 26;
        int ERROR = 27;
        int UPLOAD_PROFILE = 28;
        int GO_TO_RATING = 29;
        int ADD_RATING = 30;
        int GIVE_COMPLAINT = 31;
        int ADD_COMPLAINT = 32;
        int CLICK_PHOTO = 33;
        int PLACE_ORDER = 34;
        int BHAJNI_ACTION = 35;
        int MASALA_ACTION = 36;
        int CASH_ON_DELIVERY = 37;
        int READYMADE_ACTION = 38;
        int BUY_NOW_ENABLE_FALSE = 39;
        int CLICKFALSE_ACTION = 40;
        int LOGIN_POPUP = 41;
        int COD = 42;
        int ONLINE_PAYMENT = 43;

    }

    public interface Grams {
        double GRAMS_50 = 0.05;
        double GRAMS_100 = 0.10;
        double GRAMS_150 = 0.15;
        double GRAMS_200 = 0.20;
        double GRAMS_250 = 0.25;
        double GRAMS_300 = 0.30;
        double GRAMS_350 = 0.35;
        double GRAMS_400 = 0.40;
        double GRAMS_450 = 0.45;
        double GRAMS_500 = 0.50;
        double GRAMS_550 = 0.55;
        double GRAMS_600 = 0.60;
        double GRAMS_650 = 0.65;
        double GRAMS_700 = 0.70;
        double GRAMS_750 = 0.75;
        double GRAMS_800 = 0.80;
        double GRAMS_850 = 0.85;
        double GRAMS_900 = 0.90;
        double GRAMS_950 = 0.95;
        double GRAMS_1000 = 1;

    }

    public interface StatusCode {
        int SUCCESS = 200;
        int EMPTY_LIST = 204;
        int UNAUTHORIZED_USER = 401;
        int INTERNAL_ERROR = 500;
        int BAD_REQUEST = 400;
        int FAILED = 201;

    }

    public interface ComplaintType {
        int BadQualityProduct = 1;
        int WrongProduct = 2;
        int Other = 3;
    }

    public interface BroadCastKey {
        String USER_DATA = "user_data";
        String AREA_CHANGE = "area_change";
        String UPDATE_USER_ADDRESS = "update_user_address";
        String PIC_UPLOAD = "pic_upload";
        String SAVE_FOR_LATTER = "save_for_latter";
        String PRODUCT_DETAILED = "product_detailed";
        String GRAIN_IDS = "grain_ids";
        String QUANTITY = "quantity";
        //JM
        String ADD_TO_CART = "add_to_cart";
        String WISH_LIST = "wish_list";
        String REMOVE_WISHLIST = "remove_wishlist";
        String PRODUCT_LIST = "product_list";
        String CART_COUNT = "cart_count";
        String ADDRESS_CHANGE = "address_change";
        String SUBSCRIPTION = "subscription";
        String EDIT_USER_ADDRESS = "edit_user_address";
    }


    public interface Actor {
        int STARTUP = 1;
        int INVESTOR = 2;
        int CO_FOUNDER = 3;
        int MFG_BUSINESS = 4;
    }

    public interface ProductType {
        int NONVEG = 1;
        int VEG = 2;
        int THALI = 3;
    }

    public interface CancelReason {
        int ORDERPLACED_BY_MISTAKE = 1;
        int DELIVERY_TIME_MORE = 2;
        int REASON_THREE = 3;
        int REASON_FOUR = 4;
        int OTHER_REASON = 5;
    }

    public interface AddressType {
        int HOME = 1;
        int OFFICE = 2;
        int OTHER = 2;
    }

    public interface GrindingType {
        int FINE = 1;
        int MEDIUM = 2;
        int COURSE = 3;

    }

    public interface Documents {
        int PHOTO_LOGO = 1;
        int AWORD_DOCUMENT = 2;
        int PITCHDECK = 3;
        int RESUME = 4;
        int PRESENTATION = 5;
        int OFFERING = 6;
    }

    public interface OrderStatus {
        int INITIAL_ORDER_STATUS = 1;
        int PASS_ORDER_STATUS = 2;
        int ACCEPT_ORDRER_STATUS = 3;
        int INPROCESS_ORDER_STATUS = 4;
        int COMPLETEBYDP_ORDER_STATUS = 5;
        int PICKUP_ORDER_STATUS = 6;
        int DELIVERED_ORDER_STATUS = 4;
        int CANCLE_ORDER_STATUS = 5;
        int ACCEPTBYOTHER_ORDER_STATUS = 9;
    }

    public interface ContactRequestStatus {
        int ContactRequestAccept = 1;
        int ContactRequestReject = 2;
    }

    public interface Banner {
        int Default = 1;
        int Category = 2;
        int Product = 3;
    }

    public interface CategoryType {
        int COMBO = 0;
        int FISH = 1;
        int MUTTON = 2;
        int CHICKEN = 3;
        int CRAB = 4;
        int EGGS = 5;
    }
}
