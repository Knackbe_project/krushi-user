package com.krushi.krushi.utility;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;
import com.krushi.krushi.R;


public class Utils {

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showSnackBar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.show();

    }

    public static void ClearSharedPreference(Context context) {
        AppPreferences.setIsServiceAvailable(context, false);
        AppPreferences.setToken(context, "");
        AppPreferences.setLastName(context, "");
        AppPreferences.setAreaCode(context, 0);
        AppPreferences.setIsLogin(context, false);
        AppPreferences.setEmailId(context, "");
        AppPreferences.setMobileNo(context, "");
        AppPreferences.setIsServiceAvailable(context, false);
        AppPreferences.setIsLogin(context, false);
        AppPreferences.setCartCount(context, 0);
    }

//    public static void LogOut(Context context){
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        //   builder = new AlertDialog.Builder(getActivity());
//        LayoutInflater inflater = getLayoutInflater();
//        View dialogView = LayoutInflater.inflate(R.layout.logout_alert_box, null);
//        builder.setView(dialogView);
//
//        Button btn_positive = (Button) dialogView.findViewById(R.id.dialog_positive_btn);
//        Button btn_negative = (Button) dialogView.findViewById(R.id.dialog_negative_btn);
//        ImageView cross_close = (ImageView) dialogView.findViewById(R.id.cross_close);
//        dialog = builder.create();
//        btn_positive.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().finish();
//                Utils.ClearSharedPreference(getActivity());
//                Intent intent = new Intent(getActivity(), LoginActivity.class);
//                startActivity(intent);
//            }
//        });
//        btn_negative.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.cancel();
//            }
//        });
//        cross_close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.cancel();
//            }
//        });
//        dialog.show();
//    }


    public static void LogOut(final Context context) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.logout_alert);

        Button btn_positive = (Button) dialog.findViewById(R.id.dialog_positive_btn);
        Button btn_negative = (Button) dialog.findViewById(R.id.dialog_negative_btn);
        ImageView cross_close = (ImageView) dialog.findViewById(R.id.cross_close);

        btn_negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Utils.ClearSharedPreference(context);
               // Intent intent = new Intent(context, LoginActivity.class);
               // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
             //   context.startActivity(intent);
            }
        });

        cross_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            //use a log message
            Log.d("", "BadTokenException", e);
        }

    }


}
