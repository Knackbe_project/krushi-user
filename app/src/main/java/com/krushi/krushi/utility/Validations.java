package com.krushi.krushi.utility;

import android.text.TextUtils;
import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validations {
    public static boolean isvalidname(String fristName) {


//        fristName.setFilters(new InputFilter[] {
//                new InputFilter() {
//                    @Override
//                    public CharSequence filter(CharSequence cs, int start,
//                                               int end, Spanned spanned, int dStart, int dEnd) {
//                        // TODO Auto-generated method stub
//                        if(cs.equals("")){ // for backspace
//                            return cs;
//                        }
//                        if(cs.toString().matches("[a-zA-Z ]+")){
//                            return cs;
//                        }
//                        return "";
//                    }
//                }
//        });

        return true;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    public static boolean isValidUserName(String userName) {
        final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";
        if (userName.contains("@")) {
            Pattern pattern;
            Matcher matcher;
            pattern = Pattern.compile(EMAIL_PATTERN);
            matcher = pattern.matcher(userName);
            return matcher.matches();
        } else {
            if (userName.length() != 10) {
                return Patterns.PHONE.matcher(userName).matches();
            }
        }
        return true;
    }

    public static boolean isEmailValid(String emailAddress) {
        return Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
    }

    public static boolean isMobileValid(String phone) {
        if (phone.length() != 10) {
            return false;
        } else {
            return Patterns.PHONE.matcher(phone).matches();
        }
    }

    public static boolean isPasswordValid(String password) {
        return password.length() > 5;
    }


    public static boolean isOTPValid(String password) {
        return password.length() == 4;
    }
}

